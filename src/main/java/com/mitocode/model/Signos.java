package com.mitocode.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "signos")
public class Signos {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idSigno;
    @Column(name = "temperatura")
    private String temperatura;
    @Column(name = "pulso")
    private String pulso;
    @Column(name = "ritmo")
    private String ritmo;
    @Column(name = "fecha")
    private LocalDateTime fecha;
    @ManyToOne
    @JoinColumn(name = "id_paciente")
    private Paciente paciente;

    public Integer getIdSigno() {
        return idSigno;
    }

    public void setIdSigno(Integer idSigno) {
        this.idSigno = idSigno;
    }

    public String getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(String temperatura) {
        this.temperatura = temperatura;
    }

    public String getPulso() {
        return pulso;
    }

    public void setPulso(String pulso) {
        this.pulso = pulso;
    }

    public String getRitmo() {
        return ritmo;
    }

    public void setRitmo(String ritmo) {
        this.ritmo = ritmo;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }
}
